// Copyright 2022 Haute école d'ingénierie et d'architecture de Fribourg

/****************************************************************************
 * @file Calculator.h
 *
 * @brief Header of Calculator.cpp
 *
 * @author Yann Niclass <yann.niclass@hes-so.ch>
 *
 * @date 2022-03-02
 * @version 1.0.0
 ***************************************************************************/

#ifndef TP01_CALCULATOR_H
#define TP01_CALCULATOR_H

class Calculator {

    //enumeration & constants
    enum Operators {Add = '+', Sub = '-', Div = '/', Mul = '*'};

public:
    Calculator(); //Constructor
    virtual ~Calculator() = default; //Destructor
    float calculate(float &a, float &b, char op);

protected:
    float result; //store result to send it back to main
    float mem; //store the result for next operations
};
#endif //TP01_CALCULATOR_H
// Copyright 2022 Haute école d'ingénierie et d'architecture de Fribourg

/****************************************************************************
 * @file Calculator.cpp
 *
 * @brief File executing main operations of the calculator
 *
 * @author Yann Niclass <yann.niclass@hes-so.ch>
 *
 * @date 2022-03-02
 * @version 1.0.0
 ***************************************************************************/

#include "Calculator.h"

// Constructor
Calculator::Calculator() : result(0.0), mem(0.0) {}

// Calculate Function
float Calculator::calculate(float &a, float &b, char op){

    result = 0.0;
    mem += a;


    //executing the operations following the operator
    switch (op) {
        case Add :
            result = mem + b;
            break;
        case Sub:
            result = mem - b;
            break;
        case Mul:
            result = mem * b;
            break;
        case Div:
            result = mem / b;
            break;
        default:
            break;
    }
    mem = result;
    return result;
};

// Copyright 2022 Haute école d'ingénierie et d'architecture de Fribourg

/****************************************************************************
 * @file main.cpp
 *
 * @brief Main program executing the Calculator
 *
 * @author Yann Niclass <yann.niclass@hes-so.ch>
 *
 * @date 2022-03-02
 * @version 1.0.0
 ***************************************************************************/

#include <iostream>
#include "Calculator.h"



int main(int argc, char** argv)
{
    //enumeration & constants
    enum Operators {Add = '+', Sub = '-', Div = '/', Mul = '*', Hash = '#'};

    //new calculator object
    Calculator calc;

    //variables
    float a, b;
    char op;
    float * p; //dummy pointer for the exercise
    p = &a;



    std::cout << "Enter first value :" << std::endl;
    std::cin >> a;
    while (std::cin.fail())
    {
        std::cin.clear();
        std::cin.ignore();
        std::cout << "Not a valid number. Please reenter: ";
        std::cin >> *p;
    }

    //infinite loop permitting executing as many operations we want
    while(true) {

        std::cout << "Enter an operator :" << std::endl;
        std::cin >> op;
        while (op != '+' && op != '-' && op != '*' && op != '/' && op != '#') {
            std::cin.clear();
            std::cin.ignore();
            std::cout << "Not a valid operator. Please reenter: ";
            std::cin >> op;
        }
        if(op == Hash) {
            return 0;
        }

        std::cout << "Enter second value :" << std::endl;
        std::cin >> b;
        while (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore();
            std::cout << "Not a valid number. Please reenter: ";
            std::cin >> b;
        }

        switch (op) {
            case Add:
                std::cout << calc.calculate(a, b, Add);
                std::cout << "\n";
                a = 0;
                break;
            case Sub:
                std::cout << calc.calculate(a, b, Sub);
                std::cout << "\n";
                a = 0;
                break;
            case Mul:
                std::cout << calc.calculate(a, b, Mul);
                std::cout << "\n";
                a = 0;
                break;
            case Div:
                //Prevent division by zero
                if(b == 0) {
                    std::cout << "Division by 0 is not possible.";
                    std::cout << "\n";
                    break;
                }
                std::cout << calc.calculate(a, b, Div);
                std::cout << "\n";
                a = 0;
                break;
            default:
                break;
        }
    }
}